# Changelog

## 0.2.5 - dnd5e 2.4.1 final

* Set the maximum dnd5e version to 2.4.1 until I can update things to match the new sheet syntax

## 0.2.4

* Changed rendering hook to sync, so that Item Macro (and other modules) can properly add functionality to the items
* Fixed a CSS styling issue (presumably with dnd5e 2.1.x) that caused scrolling weirdness

## 0.2.3

Fixed a minor bug that caused inventory item uses not to render

## 0.2.2 - dnd5e 2.1.x support

* Added support for dnd5e 2.1.x sheets
* Removed the ability to edit the current spell slot count in the favorite spells display.  It was always buggy in the first place due to the way the input is handled.  

## 0.2.1 - dnd5e 2.0.3 final

* Set the maximum dnd5e version to 2.0.3 until I can update things to match the new sheet syntax
* Remove the favorite icon from active effects on the sheet (hopefully I can add it properly added after dnd5e 2.1.0 support)

## 0.2.0 - Icon customization

Added the ability to customize both the icon and color used for favorites via a pair of settings.  

## 0.1.0 - Initial relase

Initial release of the module, supporting adding a display of favorites in the dnd5e system

### Features

* Inserts a favorites section into main page
* Inserts toggle buttons in the inventory/features/spellbooks to mark things as favorites

## 
